import React from "react";
import ReactDOM from "react-dom";
import { applyMiddleware, createStore } from "redux";
import { Provider } from "react-redux";
import { Router, Route, Switch, hashHistory } from "react-router";
import createLogger from "redux-logger";

import reducers from "./reducers";

// components
import App from "./app/App";
import Photos from "./photos/Photos";

// logger
const logger = createLogger({ duration: true });

//store
var store = createStore(reducers, applyMiddleware(logger));

ReactDOM.render(
  <Provider store={store}>
    <Router history={hashHistory}>
      <App>
        <Route path="/" component={Photos} />
      </App>
    </Router>
  </Provider>,
  document.getElementById("root")
);
