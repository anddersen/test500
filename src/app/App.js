import React, { Component } from "react";
const { connect } = require("react-redux");

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return <div>{this.props.children}</div>;
  }
}

export default connect(state => ({}), {})(App);
