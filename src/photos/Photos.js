import React, { Component } from "react";
import InfiniteScroll from "react-infinite-scroll-component";
const { connect } = require("react-redux");

class Photos extends Component {
  constructor(props) {
    super(props);
    this.state = {
      items: []
    };
  }

  componentDidMount() {
    this.setState({
      items: this.state.items.concat(
        <div key={this.state.items.length} style={{ height: 1500 + "px" }}>
          ss
        </div>
      )
    });
  }
  next() {
    console.log('next');
    setTimeout(() => {
      this.setState({
        items: this.state.items.concat(
          <div key={this.state.items.length} style={{ height: 100 + "px" }}>
            ss
          </div>
        )
      });
    }, 500);
  }

  render() {
    return (
      <InfiniteScroll
        next={()=>this.next()}
        hasMore={true}
        loader={<h4>Loading...</h4>}
      >
        {this.state.items}
      </InfiniteScroll>
    );
  }
}

export default connect(state => ({}), {})(Photos);
