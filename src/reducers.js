import { combineReducers } from "redux";

import photos from "./photos/Photos-reducer";

export default combineReducers({
  photos: photos
});
