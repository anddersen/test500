"use strict";

const webpack = require("webpack");
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const path = require("path");

module.exports = {
  entry: ["babel-polyfill", path.resolve(__dirname + "/src/app")],
  output: {
    path: path.resolve(__dirname + "/public/build/"),
    publicPath: "/build/",
    filename: "bundle.js"
  },
  devtool: "evil",

  plugins: [
    new ExtractTextPlugin({ filename: "styles.css", allChunks: false })
  ],

  module: {
    loaders: [
      {
        test: /\.js$/,
        exclude: [/node_modules/, /public/],
        loader: "babel-loader",
        query: {
          presets: ["es2015", "stage-2", "react"]
        }
      },
      {
        test: /\.css$/,
        loader: "style!css"
      },
      {
        test: /\.scss$/,
        loader: ExtractTextPlugin.extract({
          fallback: "style-loader",
          use: [
            "css-loader",
            "autoprefixer-loader",
            "resolve-url-loader",
            "sass-loader",
            {
              loader: "sass-resources-loader",
              options: {
                resources: [
                  "./src/sass/settings/global.scss",
                  "./src/sass/settings/normalize.scss"
                ]
              }
            }
          ]
        })
      },
      {
        test: /\.(png|woff|woff2|eot|ttf|svg)([\?]?.*)$/,
        loader: "file-loader"
      }
    ]
  }
};
